#ifdef WIN32
	#define WIN32_LEANANDMEAN
	#include <Windows.h>

	#define EXPORT __declspec(dllexport)
#else
	#define EXPORT
#endif

#include "VCMP.h"
#include <math.h>

#ifdef __GNUC__
	#define strcmpi strcasecmp
#endif
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <wchar.h>

#include <iostream>
#include <map>