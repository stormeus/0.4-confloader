// Main include
#include "main.h"

// Console stuff
#include "ConsoleUtils.h"

// TinyXML
#include "tinyxml2.h"

// Unfortunately, stdlib
#include <cstdlib>
#include <iostream>
#include <fstream>

// Global functions structure
PluginFuncs * globalFuncs;

// Utility functions
inline unsigned int IsTrue( const char * expression )
{
	if( strcmpi( expression, "1" ) == 0 )
		return 1;
	else if( strcmpi( expression, "true" ) == 0 )
		return 1;
	else if( strcmpi( expression, "yes" ) == 0 )
		return 1;
	else if( strcmpi( expression, "on" ) == 0 )
		return 1;
	else
		return 0;
}

// Converts ARGB to unsigned int
inline unsigned int ARGB( unsigned char r, unsigned char g, unsigned char b, unsigned char a )
{
	unsigned int dwColour;
	dwColour  = ( a << 24 );
	dwColour += ( ( r & 0xFF ) << 16 );
	dwColour += ( ( g & 0xFF ) << 8 );
	dwColour += ( b & 0xFF );

	return dwColour;
}

uint8_t OnInitServer( void )
{
	// Create a new document instance
	tinyxml2::XMLDocument * doc = new tinyxml2::XMLDocument;

	// Output a message
	OutputMessage( "Loaded server.conf Loader for 0.4 by Stormeus" );

	// See if server.conf exists
	std::ifstream fCheck( "server.conf" );
	if( !fCheck )
	{
		OutputError( "server.conf does not exist." );
		return 1;
	}
	else if( !fCheck.is_open() )
	{
		OutputError( "server.conf does not exist." );
		fCheck.close();

		return 1;
	}
	else
		fCheck.close();

	// Try to load server.conf
	try
	{
		// Load server.conf
		doc->LoadFile( "server.conf" );

		// Done with that
		OutputMessage( "   >> Loaded server.conf for parsing" );

		// Allocate a variable for the settings block
		tinyxml2::XMLElement * settings;

		// Get the <Settings>...</Settings> block
		settings = doc->FirstChildElement( "Settings" );

		// Allocate variables for getting text and elements
		tinyxml2::XMLElement * tmpElem;
		
		// Start reading settings
		{
			// Get the server name
			tmpElem = settings->FirstChildElement( "ServerName" );
			if( tmpElem && tmpElem->GetText() )
				globalFuncs->SetServerName( tmpElem->GetText() );

			// Get the max. number of players
			tmpElem = settings->FirstChildElement( "MaxPlayers" );
			if( tmpElem )
			{
				int players;
				tmpElem->QueryIntText( &players );
				globalFuncs->SetMaxPlayers( players );
			}

			// Get the listen port
			tmpElem = settings->FirstChildElement( "ListenPort" );
			if( tmpElem )
				OutputWarning( "You MUST define ListenPort in server.cfg" );

			// Get the password
			tmpElem = settings->FirstChildElement( "Password" );
			if( tmpElem && tmpElem->GetText() )
			{
				if ( tmpElem->GetText() )
				{
					const char * pText = tmpElem->GetText();
					if (strlen(pText) > 0)
					{
						char * pTmpText = strdup(pText);
						if (pTmpText)
							globalFuncs->SetServerPassword(pTmpText);

						free(pTmpText);
					}
				}
			}

			// Get the RCON password
			tmpElem = settings->FirstChildElement( "AdminPassword" );
			if( tmpElem )
				OutputWarning( "AdminPassword has no effect. RCON does not exist in 0.4." );

			// See if friendly fire is enabled
			tmpElem = settings->FirstChildElement( "FriendlyFire" );
			if( tmpElem )
				globalFuncs->SetServerOption( vcmpServerOptionShootInAir, IsTrue( tmpElem->GetText() ) );

			// See if radar showing is enabled
			tmpElem = settings->FirstChildElement( "ShowOnRadar" );
			if (tmpElem) {
				globalFuncs->SetServerOption(vcmpServerOptionShowMarkers, IsTrue(tmpElem->GetText()));
				if (strcmpi(tmpElem->GetText(), "2") == 0) {
					globalFuncs->SetServerOption(vcmpServerOptionShowMarkers, true);
					globalFuncs->SetServerOption(vcmpServerOptionOnlyShowTeamMarkers, true);
				}
			}

			// See if RconPort is defined
			tmpElem = settings->FirstChildElement( "RconPort" );
			if( tmpElem )
				OutputWarning( "RconPort has no effect. RCON does not exist in 0.4." );

			// See if RconMaxUsers is defined
			tmpElem = settings->FirstChildElement( "RconMaxUsers" );
			if( tmpElem )
				OutputWarning( "RconMaxUsers has no effect. RCON does not exist in 0.4." );

			// See if WeatherDefault is defined
			tmpElem = settings->FirstChildElement( "WeatherDefault" );
			if( tmpElem )
			{
				int weather;
				tmpElem->QueryIntText( &weather );
				globalFuncs->SetWeather( weather );
			}

			// Set default time
			tmpElem = settings->FirstChildElement( "HourDefault" );
			if( tmpElem )
			{
				int hour;
				tmpElem->QueryIntText( &hour );
				globalFuncs->SetHour( hour );
			}

			tmpElem = settings->FirstChildElement( "MinuteDefault" );
			if( tmpElem )
			{
				int minute;
				tmpElem->QueryIntText( &minute );
				globalFuncs->SetMinute( minute );
			}

			// Set time rate
			tmpElem = settings->FirstChildElement( "TimeRate" );
			if( tmpElem )
			{
				int rate;
				tmpElem->QueryIntText( &rate );
				globalFuncs->SetTimeRate( rate );
			}

			// Set gamemode name
			tmpElem = settings->FirstChildElement( "GameModeName" );
			if( tmpElem && tmpElem->GetText() )
				globalFuncs->SetGameModeText( tmpElem->GetText() );

			// See if ConsoleOutput is defined
			tmpElem = settings->FirstChildElement( "ConsoleOutput" );
			if( tmpElem )
				OutputWarning( "ConsoleOutput has no effect." );

			// See if Announce is defined
			tmpElem = settings->FirstChildElement( "Announce" );
			if( tmpElem )
				OutputWarning( "Announce has no effect." );

			// Set camera stuff
			{
				// Allocate x, y, and z coordinates
				float x, y, z;

				// Get the player position
				tmpElem = settings->FirstChildElement( "PlayerPos" );
				if( tmpElem )
				{
					x       = tmpElem->DoubleAttribute("x");
					y       = tmpElem->DoubleAttribute("y");
					z       = tmpElem->DoubleAttribute("z");

					if( x && y && z )
						globalFuncs->SetSpawnPlayerPosition( x, y, z );
				}

				// Get the camera position
				tmpElem = settings->FirstChildElement( "CamPos" );
				if( tmpElem )
				{
					x = tmpElem->DoubleAttribute("x");
					y = tmpElem->DoubleAttribute("y");
					z = tmpElem->DoubleAttribute("z");

					if( x && y && z )
						globalFuncs->SetSpawnCameraPosition( x, y, z );
				}

				// Get what the camera is looking at
				tmpElem = settings->FirstChildElement( "CamLook" );
				if( tmpElem )
				{
					x = tmpElem->DoubleAttribute( "x" );
					y = tmpElem->DoubleAttribute( "y" );
					z = tmpElem->DoubleAttribute( "z" );

					if( x && y && z )
						globalFuncs->SetSpawnCameraLookAt( x, y, z );
				}
			}

			// Set world boundaries
			tmpElem = settings->FirstChildElement( "WorldBoundaries" );
			if( tmpElem )
			{
				float maxX, minX, maxY, minY;
				maxX = tmpElem->DoubleAttribute("MaxX");
				minX = tmpElem->DoubleAttribute("MinX");
				maxY = tmpElem->DoubleAttribute("MaxY");
				minY = tmpElem->DoubleAttribute("MinY");

				if( maxX && minX && maxY && minY )
					globalFuncs->SetWorldBounds( maxX, minX, maxY, minY );
			}

			// Check R2 features
			{
				// Check if syncing frame limiter
				tmpElem = settings->FirstChildElement( "SyncFrameLimiter" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionSyncFrameLimiter, IsTrue( tmpElem->GetText() ) );

				// Check if enabling frame limiter
				tmpElem = settings->FirstChildElement( "FrameLimiter" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionFrameLimiter, IsTrue( tmpElem->GetText() ) );

				// Check if enabling taxi boost jump
				tmpElem = settings->FirstChildElement( "TaxiBoostJump" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionTaxiBoostJump, IsTrue( tmpElem->GetText() ) );

				// Check if enabling drive on water
				tmpElem = settings->FirstChildElement( "DriveOnWater" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionDriveOnWater, IsTrue( tmpElem->GetText() ) );

				// Check if enabling fast switch
				tmpElem = settings->FirstChildElement( "FastSwitch" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionFastSwitch, IsTrue( tmpElem->GetText() ) );

				// Check if disabling drive by
				tmpElem = settings->FirstChildElement( "DisableDriveBy" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionDisableDriveBy, IsTrue( tmpElem->GetText() ) );

				// Check if enabling perfect handling
				tmpElem = settings->FirstChildElement( "PerfectHandling" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionPerfectHandling, IsTrue( tmpElem->GetText() ) );

				// Check if enabling flying cars
				tmpElem = settings->FirstChildElement( "FlyingCars" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionFlyingCars, IsTrue( tmpElem->GetText() ) );

				// Check if enabling jump switch
				tmpElem = settings->FirstChildElement( "JumpSwitch" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionJumpSwitch, IsTrue( tmpElem->GetText() ) );

				// Check if enabling death messages
				tmpElem = settings->FirstChildElement( "DeathMessages" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionDeathMessages, IsTrue( tmpElem->GetText() ) );
			}

			// Get gravity
			tmpElem = settings->FirstChildElement( "Gravity" );
			if( tmpElem )
			{
				float gravity;
				tmpElem->QueryFloatText( &gravity );
				globalFuncs->SetGravity( gravity );
			}

			// Get game speed
			tmpElem = settings->FirstChildElement( "GameSpeed" );
			if( tmpElem )
			{
				float speed;
				tmpElem->QueryFloatText( &speed );
				globalFuncs->SetGameSpeed( speed );
			}

			// Get water level
			tmpElem = settings->FirstChildElement( "WaterLevel" );
			if( tmpElem )
			{
				float water;
				tmpElem->QueryFloatText( &water );
				globalFuncs->SetWaterLevel( water );
			}

			// 0.4 settings
			{
				// Shoot in air?
				tmpElem = settings->FirstChildElement( "ShootInAir" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionShootInAir, IsTrue( tmpElem->GetText() ) );

				// Join messages?
				tmpElem = settings->FirstChildElement( "JoinMessages" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionJoinMessages, IsTrue( tmpElem->GetText() ) );
				
				// Showing nametags?
				tmpElem = settings->FirstChildElement( "ShowNametags" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionShowNameTags, IsTrue( tmpElem->GetText() ) );

				// Stunt bike mode?
				tmpElem = settings->FirstChildElement( "StuntBike" );
				if( tmpElem )
					globalFuncs->SetServerOption( vcmpServerOptionStuntBike,  IsTrue( tmpElem->GetText() ) );
			}
		}

		// Done with that
		OutputMessage( "   >> Applied settings" );

		// Check if they defined any ammunations or ammu-weapons
		tmpElem = doc->FirstChildElement( "Ammunation" );
		if( tmpElem )
			OutputWarning( "Ammunation has no effect. Ammunations must be completely scripted." );

		tmpElem = doc->FirstChildElement( "A_Weapon" );
		if( tmpElem )
			OutputWarning( "A_Weapon has no effect. Ammunation weapons must be completely scripted." );

		// Allocate variables for class loading
		unsigned char team;
		unsigned short skin;
		float x, y, z, angle;
		unsigned char weapon1, ammo1;
		unsigned char weapon2, ammo2;
		unsigned char weapon3, ammo3;
		unsigned char r, g, b, a;
		unsigned int colour;

		// Allocate variables for tracking errors
		bool raisedNoClassColor, raisedNoPickupWorld, raisedNoVehWorld;

		// Initialize variables to fix runtime errors
		raisedNoClassColor = false;
		raisedNoPickupWorld = false;
		raisedNoVehWorld = false;

		// Load all classes
		tmpElem = doc->FirstChildElement( "Class" );
		while( tmpElem )
		{
			// Reset default colors
			r = 255;
			g = 255;
			b = 255;
			a = 255;

			// Get what we need
			team    = tmpElem->IntAttribute("team");
			skin    = tmpElem->IntAttribute("skin");

			x       = tmpElem->DoubleAttribute("x");
			y       = tmpElem->DoubleAttribute("y");
			z       = tmpElem->DoubleAttribute("z");
			angle   = tmpElem->DoubleAttribute("angle");
			#ifdef RADIAN
				//angle   = angle * (PI / 180);
			#endif
			weapon1 = tmpElem->IntAttribute("weapon1");
			ammo1   = tmpElem->IntAttribute("ammo1");
			weapon2 = tmpElem->IntAttribute("weapon2");
			ammo2   = tmpElem->IntAttribute("ammo2");
			weapon3 = tmpElem->IntAttribute("weapon3");
			ammo3   = tmpElem->IntAttribute("ammo3");

			// We have the color red defined
			if( tmpElem->Attribute( "r" ) )
			{
				r = tmpElem->IntAttribute("r");
				g = tmpElem->IntAttribute("g");
				b = tmpElem->IntAttribute("b");
				a = tmpElem->IntAttribute("a");
			}
			else if( !raisedNoClassColor )
			{
				OutputWarning( "Please define a colour for your class. (See docs)" );
				raisedNoClassColor = true;
			}
			
			// Convert color to unsigned int
			colour  = ARGB( r, g, b, a );

			// Add class
			globalFuncs->AddPlayerClass(
				team,
				colour,
				skin,
				x,
				y,
				z,
				angle,
				weapon1,
				ammo1,
				weapon2,
				ammo2,
				weapon3,
				ammo3
			);

			// Go to next element
			tmpElem = tmpElem->NextSiblingElement("Class");
		}

		// Done with that
		OutputMessage( "   >> Loaded classes" );

		// Create variables for vehicle loading
		int world, model, colour1, colour2, vehID;

		// Load all vehicles
		tmpElem = doc->FirstChildElement( "Vehicle" );
		float angleX, angleY, angleZ;
		while( tmpElem )
		{

			// Load what we need
			model   = tmpElem->IntAttribute("model");
			x       = tmpElem->FloatAttribute("x");
			y       = tmpElem->FloatAttribute("y");
			z       = tmpElem->FloatAttribute("z");
			angleX  = tmpElem->FloatAttribute("rotx");
			angleY  = tmpElem->FloatAttribute("roty");
			angleZ  = tmpElem->FloatAttribute("rotz");
			colour1 = tmpElem->IntAttribute("col1");
			colour2 = tmpElem->IntAttribute("col2");
			world	= tmpElem->IntAttribute("world");
			
			// World is not defined
			if( !world )
			{
				if ( !raisedNoVehWorld ) {
					OutputWarning( "Please define a world for your vehicle. (See docs)" );
					raisedNoVehWorld = true;
				}

				world = 1;
			}

			// Add vehicle
			vehID = globalFuncs->CreateVehicle(
				model,
				world,
				x,
				y,
				z,
				angleZ,
				colour1,
				colour2
			);
			globalFuncs->SetVehicleSpawnRotationEuler( vehID, angleX, angleY, angleZ );
			globalFuncs->SetVehicleIdleRespawnTimer( vehID, 180000 );
			globalFuncs->RespawnVehicle( vehID );
			
			// Go to next element
			tmpElem = tmpElem->NextSiblingElement("Vehicle");
		}

		// Done with that
		OutputMessage( "   >> Loaded vehicles" );

		// Load all pickups
		tmpElem = doc->FirstChildElement( "Pickup" );
		while( tmpElem )
		{
			// Load what we need
			model = tmpElem->IntAttribute( "model" );
			x     = tmpElem->DoubleAttribute("x");
			y     = tmpElem->DoubleAttribute("y");
			z     = tmpElem->DoubleAttribute("z");
			world	= tmpElem->IntAttribute("world");
			
			// World is not defined
			if( !world )
			{
				if ( !raisedNoPickupWorld ) {
					OutputWarning( "Please define a world for your pickups. (See docs)" );
					raisedNoPickupWorld = true;
				}

				world = 1;
			}

			// Create pickup
			globalFuncs->CreatePickup(
				model,
				world,
				0,
				x,
				y,
				z,
				255,
				0
			);

			// Go to next element
			tmpElem = tmpElem->NextSiblingElement("Pickup");
		}

		// Done with that
		OutputMessage( "   >> Loaded pickups" );

		// Done
		OutputMessage( "Loaded server.conf successfully" );
	}
	catch( ... )
	{
		OutputError( "Failed to parse server.conf" );
	}

	// Delete the instance we created
	delete doc;

	// Done here
	return 1;
}

extern "C" EXPORT unsigned int VcmpPluginInit( PluginFuncs* givenPluginFuncs, PluginCallbacks* givenPluginCalls, PluginInfo* givenPluginInfo )
{
    givenPluginInfo->apiMajorVersion = PLUGIN_API_MAJOR;
    givenPluginInfo->apiMinorVersion = PLUGIN_API_MINOR;

	// Hook server events as needed
	givenPluginCalls->OnServerInitialise = OnInitServer;

	// Set the global functions struct
	globalFuncs = givenPluginFuncs;

	// Done
	return 1;
}
